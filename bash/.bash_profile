
source ~/.aliases
source ~/.bashrc

export PYTHONPATH="/usr/local/lib/python2.7/site-packages"

if [ -f `brew --prefix`/etc/bash_completion ]; then
    . `brew --prefix`/etc/bash_completion
fi

gcce(){
gcc -Wall -Werror $1 -o $2
}

gccc(){
clear && gcc -Wall -Werror $1 -o $2
}

function cdl { cd $1; ls;}
test -r /sw/bin/init.sh && . /sw/bin/init.sh

export CLICOLOR=1
export LSCOLORS=

# enables color in the terminal bash shell 
export CLICOLOR=1  
# sets up the color scheme for list
export LSCOLORS=gxfxcxdxbxegedabagacad  
# enables color for iTerm 
export TERM=xterm-color  
# Color Prompt 
export PS1="\[\e[36;1m\]\u@\[\e[32;1m\]\W \[\e[0m\]" 

# sets up proper alias commands when called
alias ls='ls -G'
alias ll='ls -hl'


#simple compile
compile () {
  FILE=`echo $1 | cut -d'.' -f 1` # incase I accidentally say asdf.cpp
  if [ -e $FILE.c ]
  then
    gcc -Wall -Werror -std=c99 -g $FILE.c -o $FILE
  fi
}

export XUGGLE_HOME="/usr/local/xuggler"

export TRACKER_HOME="/usr/local/tracker"


##
# Your previous /Users/carlneuhaus/.bash_profile file was backed up as /Users/carlneuhaus/.bash_profile.macports-saved_2012-10-28_at_20:47:53
##

# MacPorts Installer addition on 2012-10-28_at_20:47:53: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/carlneuhaus/.bash_profile file was backed up as /Users/carlneuhaus/.bash_profile.macports-saved_2013-05-05_at_15:47:46
##

# MacPorts Installer addition on 2013-05-05_at_15:47:46: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/carlneuhaus/.bash_profile file was backed up as /Users/carlneuhaus/.bash_profile.macports-saved_2014-08-04_at_14:49:40
##

# MacPorts Installer addition on 2014-08-04_at_14:49:40: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


# Open out of terminal in sublime-text
# Usage: sublime <command>

function sublime()
{
	"$@" > ~/$$.txt
	subl ~/$$.txt
	rm -f ~/$$.txt
}

